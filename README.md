# Spell French Numbers

<https://french-numbers.adamts.me>

Convert a number to its French spelling.

## Info

This tool will:

- Hyphenate all words (eg. 1 234 -> mille-deux-cent-trente-quatre)
- Pluralize words when necessary (eg. 2 000 000 -> deux-millions)

## Building

To build for development, run:

```sh
yarn dev
```

To build for production, run:

```sh
yarn build
```

## License

This project is licensed under the GNU General Public License, Version 3.0
([LICENSE](LICENSE) or <https://www.gnu.org/licenses/gpl-3.0.en.html>).
